import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Navbar from './components/UI/navbar/navbar';
import Noticia from './components/noticias/noticia/noticia';
import Noticias from './components/noticias/noticias';
import Escuelas from './components/escuelas/escuelas';
import Curso from './components/cursos/curso/curso'
import Cursos from './components/cursos/cursos';
import Documento from './components/documentos/documento/documento';
import Documentos from './components/documentos/documentos';
import Alumnos from './components/alumnos/alumnos';
import DetalleAlumno from './components/alumnos/detalleAlumno';
import NuevoAlumno from './components/alumnos/nuevoAlumno';
import ModificarEscuela from './components/administracion/escuelas/modificarEscuela';
import NuevaEscuela from './components/administracion/escuelas/nuevaEscuela'
import Administracion from './components/administracion/administracion';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Switch>
          <Route path='/alumnos/nuevo' component={NuevoAlumno} />
          <Route path='/alumnos/:idAlumno' component={DetalleAlumno} />
          <Route path='/escuelas/:numeroEscuela/curso/:idCurso' component={Alumnos} />
          <Route path='/escuelas/:numeroEscuela' component={Cursos} />
          <Route path='/escuelas' component={Escuelas} />
          <Route path='/administracion/nuevaEscuela' component={NuevaEscuela} />
          <Route path='/administracion/seleccionarEscuela' render={() => (<Escuelas modificar="true" />)} />
          <Route path='/administracion/:numeroEscuela/curso/:idCurso' component={Curso} />
          <Route path='/administracion/:numeroEscuela' component={ModificarEscuela} />
          <Route path='/administracion' component={Administracion} />
          <Route path='/noticias/:idNoticia' component={Noticia} />
          <Route path='/noticias' component={Noticias} />
          <Route path='/documentos/:idDocumento' component={Documento} />
          <Route path='/documentos' component={Documentos} />
        </Switch>
      </div>
    );
  }
}

export default App;
