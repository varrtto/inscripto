import React from 'react';
import { Button } from 'react-bootstrap';

const guardarCancelar = (props) => {
  return (
    <>
      <Button
        block variant="primary"
        type="submit"
        onClick={props.guardar}> Guardar
      </Button>
      <Button
        block variant="danger"
        type="submit"
        onClick={props.cancelar}> Cancelar
      </Button>
    </>
  )
}

export default guardarCancelar;
