import React from 'react';
import { Link, withRouter } from 'react-router-dom'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

const navbar = (props) => {
  return (
    <Navbar sticky='top' collapseOnSelect expand="lg" bg='light' variant='light'>
      <Navbar.Brand href="/">Inscripto</Navbar.Brand>
      <Navbar.Toggle aria-controls="colapsa" />
      <Navbar.Collapse id="colapsa">
        <Nav fill justify className="mr-auto">
          <Nav.Link
            eventKey={1}
            active={props.location.pathname === '/'}
            as={Link}
            to='/' >Inicio </Nav.Link>
          <Nav.Link
            eventKey={2}
            active={props.location.pathname === '/escuelas'}
            as={Link}
            to='/escuelas'> Escuelas </Nav.Link>
          <Nav.Link
            eventKey={3}
            active={props.location.pathname === '/noticias'}
            as={Link}
            to='/noticias'> Noticias </Nav.Link>
          <Nav.Link
            eventKey={4}
            active={props.location.pathname === '/documentos'}
            as={Link}
            to='/documentos'> Documentos </Nav.Link>
          <NavDropdown title="Administracion" id="collasible-nav-dropdown">
            <NavDropdown.Item eventKey={5} as={Link} to="/administracion/nuevaEscuela">Nueva Escuela</NavDropdown.Item>
            <NavDropdown.Item eventKey={6} as={Link} to="/administracion/seleccionarEscuela">Modificar Escuela</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default withRouter(navbar);
