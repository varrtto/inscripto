import React from 'react';

const placeholder = (props) => {
  return (
    <span style={{fontSize: '8px', position: 'absolute', display:'block', left: '17px', top:'1px', color: 'rgb(81, 110, 199)'}}>{props.children}</span>
  )
}

export default placeholder;
