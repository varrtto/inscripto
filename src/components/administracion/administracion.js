import React from 'react';
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap';

const administracion = () => {
  return (
    <>
      <Button as={Link} to="/administracion/nuevaEscuela">Nueva Escuela</Button>
      <br />
      <Button as={Link} to="/administracion/seleccionarEscuela">Modificar Escuela</Button>
    </>
  )
}

export default administracion;
