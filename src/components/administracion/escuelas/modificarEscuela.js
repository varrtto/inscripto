import React, { Component } from 'react';
import { Container, Button } from 'react-bootstrap';
import GuardarCancelar from '../../UI/Botoneras/guardarCancelar';
import Escuela from '../../escuelas/escuela/escuela';
import Cursos from '../../cursos/cursos';

class ModificarEscuela extends Component {
  onGuardarHandler = () => {
    this.props.history.push('/escuelas');
  }

  onCancelarHandler = () => {
    this.props.history.push('/escuelas');
  }

  render() {
    return (
      <Container style={{ textAlign: 'center', padding: '0px' }}>
        <Escuela datos={this.props.location} />
        <Cursos numeroEscuela />
        <Button variant='success'>+Nuevo Curso</Button>
        <br />
        <br />
        <GuardarCancelar
          guardar={this.onGuardarHandler}
          cancelar={this.onCancelarHandler} />
        <br />
      </Container>
    );
  }
}

export default ModificarEscuela;
