import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Escuela from '../../escuelas/escuela/escuela';
import GuardarCancelar from '../../UI/Botoneras/guardarCancelar';

class nuevaEscuela extends Component {
  onGuardarHandler = () => {
    this.props.history.push('/escuelas');
  }

  onCancelarHandler = () => {
    this.props.history.push('/escuelas');
  }

  render() {
    return (
      <Container style={{ padding: '0px' }}>
        <Escuela />
        <GuardarCancelar
          guardar={this.onGuardarHandler}
          cancelar={this.onCancelarHandler} />
      </Container>
    )
  }
}

export default nuevaEscuela;
