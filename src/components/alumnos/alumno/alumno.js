import React from 'react';
import { Form, Container } from 'react-bootstrap';
import Placeholder from '../../UI/placeholder/placeholder';

const alumno = (props) => {
  let datosAlumno = {
    direccion: {},
    padre: {},
    madre: {}
  }
  console.log(props.datos)
  if (props.datos) {
    datosAlumno = props.datos;
  }
  return (
    <Form>
      <Container style={{borderLeft: '6px solid pink'}}>
        <br />
        <p style={{textAlign: 'left'}}>Datos Alumno</p>
        <Form.Row>
          <Form.Group className='col-md' controlId="formGridDNI">
            <Placeholder>DNI</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.dni}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridNombre">
            <Placeholder>Nombre</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.nombre}/>
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridApellido">
            <Placeholder>Apellido</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.apellido}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridCalle">
            <Placeholder>Calle</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.direccion.calle}/>
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridNumero">
            <Placeholder>Numero</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.direccion.numero}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridTelefono">
            <Placeholder>Telefono</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.telefono}/>
          </Form.Group>
          <Form.Group className='col-md' controlId="formGridSexo">
            <div key={`inline-radio`} className="mb-3">
              <Form.Check defaultChecked={datosAlumno.sexo==='H'} name='grupoSexo' inline label="Hombre" type='radio' id={`inline-radio-Hombre`} />
              <Form.Check defaultChecked={datosAlumno.sexo==='M'} name='grupoSexo' inline label="Mujer" type='radio' id={`inline-radio-Mujer`} />
            </div>
          </Form.Group>
        </Form.Row>
      </Container>

      <Container style={{borderLeft: '6px solid lightgreen'}}>
        <p style={{textAlign: 'left'}}>Datos Padre</p>
        <Form.Row>
          <Form.Group className='col-md' controlId="formGridDNIP">
            <Placeholder>DNI</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.padre.dni}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridNombreP">
            <Placeholder>Nombre</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.padre.nombre} />
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridApellidoP">
            <Placeholder>Apellido</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.padre.apellido}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridTelefonoP">
            <Placeholder>Telefono</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.padre.telefono}/>
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridEmailP">
            <Placeholder>Email</Placeholder>
            <Form.Control type="email" defaultValue={datosAlumno.padre.email}/>
          </Form.Group>
        </Form.Row>
      </Container>

      <Container style={{borderLeft: '6px solid lightblue'}}>
        <p style={{textAlign: 'left'}}>Datos Madre</p>
        <Form.Row>
          <Form.Group className='col-md' controlId="formGridDNIM">
            <Placeholder>DNI</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.madre.dni}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridNombreM">
            <Placeholder>Nombre</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.madre.nombre}/>
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridApellidoM">
            <Placeholder>Apellido</Placeholder>
            <Form.Control type="text" defaultValue={datosAlumno.madre.apellido}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridTelefonoM">
            <Placeholder>Telefono</Placeholder>
            <Form.Control type="number" defaultValue={datosAlumno.madre.telefono}/>
          </Form.Group>

          <Form.Group className='col-md' controlId="formGridEmailM">
            <Placeholder>Email</Placeholder>
            <Form.Control type="email" defaultValue={datosAlumno.madre.email}/>
          </Form.Group>
        </Form.Row>
        <br />
      </Container>
    </Form>
  )
}

export default alumno;
