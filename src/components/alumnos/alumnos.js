import React, { Component } from 'react';
import { Container, Table, Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom'

const alumnos = [
  {
    dni: 50000000,
    nombre: 'Juan',
    apellido: 'Perez',
    sexo: 'H',
    telefono: 2313213,
    direccion: {
      calle: 'Villegas',
      numero: 434
    },
    padre: {
      dni: 30000000,
      nombre: 'Jorge',
      apellido: 'Perez',
      telefono: 2313213,
      email: 'prueba@email.com'
    },
    madre: {
      dni: 31000000,
      nombre: 'Maria',
      apellido: 'Gonzalez',
      telefono: 2313213,
      email: 'prueba@email.com'
    }
  },
  {
    dni: 50000001,
    nombre: 'Matias',
    apellido: 'Rodriguez',
    sexo: 'H',
    telefono: 2313213,
    direccion: {
      calle: 'Villegas',
      numero: 434
    },
    padre: {
      dni: 30000001,
      nombre: 'Esteban',
      apellido: 'Perez',
      telefono: 2313213,
      email: 'prueba@email.com'
    },
    madre: {
      dni: 31000001,
      nombre: 'Estela',
      apellido: 'Etcheverria',
      telefono: 2313213,
      email: 'prueba@email.com'
    }
  }, {
    dni: 50000002,
    nombre: 'Julia',
    apellido: 'Cesar',
    sexo: 'M',
    telefono: 2313213,
    direccion: {
      calle: 'Villegas',
      numero: 434
    },
    padre: {
      dni: 30000002,
      nombre: 'Nicolas',
      apellido: 'Cesar',
      telefono: 2313213,
      email: 'prueba@email.com'
    },
    madre: {
      dni: 31000002,
      nombre: 'Estefania',
      apellido: 'Garcia',
      telefono: 2313213,
      email: 'prueba@email.com'
    }
  },
];

class Alumnos extends Component {

  componentWillMount() {
    if (!this.props.location.escuela &&
      !this.props.location.anio &&
      !this.props.location.division &&
      !this.props.location.turno) {
      this.props.history.push('/escuelas');
    }
  };

  borrarAlumno = (dniAlumno) => {
    console.log(dniAlumno);
  }

  render() {
    let toAlumno = {};
    let toNuevoAlumno = {
      pathname: '/alumnos/nuevo',
      grado: {
        escuela: this.props.location.escuela,
        anio: this.props.location.anio,
        division: this.props.location.division,
        turno: this.props.location.turno
      }
    }
    let elEstilo = {
      all: 'unset',
      display: 'block'
    }
    let listaAlumnos = Object.entries(alumnos).map(alumno => {
      toAlumno = {
        pathname: '/alumnos/' + alumno[1].dni,
        grado: {
          escuela: this.props.location.escuela,
          anio: this.props.location.anio,
          division: this.props.location.division,
          turno: this.props.location.turno
        },
        alumno: alumno[1]
      }
      return (
        <tr
          key={alumno[1].dni}
          style={{ cursor: 'pointer' }}>
          <td><Link to={toAlumno} style={elEstilo}>{+alumno[0] + 1}</Link></td>
          <td><Link to={toAlumno} style={elEstilo}>{alumno[1].dni}</Link></td>
          <td><Link to={toAlumno} style={elEstilo}>{alumno[1].nombre}</Link></td>
          <td><Link to={toAlumno} style={elEstilo}>{alumno[1].apellido}</Link></td>
          <td onClick={() => this.borrarAlumno(alumno[1].dni)}>X</td>
        </tr>
      )
    });

    return (
      <Container style={{ padding: '0px' }}>
        <Container style={{ textAlign: 'left' }}>
          <Row>
            <Col>Escuela: {this.props.location.escuela}</Col>
            <Col>Turno: {this.props.location.turno}</Col>
          </Row>
          <Row>
            <Col>Año: {this.props.location.anio}</Col>
            <Col>Division: {this.props.location.division}</Col>
          </Row>
        </Container>
        <Table striped hover responsive size='sm'>
          <thead>
            <tr style={{ backgroundColor: 'lightblue' }}>
              <th>#</th>
              <th>DNI</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {listaAlumnos}
          </tbody>
        </Table>
        <Button style={{ width: '95%' }} variant="primary">
          <Link to={toNuevoAlumno} style={elEstilo}>
            Nuevo Alumno
            </Link>
        </Button>
      </Container>
    );
  }
}

export default Alumnos;
