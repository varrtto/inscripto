import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Alumno from './alumno/alumno';
import GuardarCancelar from '../UI/Botoneras/guardarCancelar';

class DetalleAlumno extends Component {
  componentWillMount() {
    if (!this.props.location.alumno || !this.props.location.grado) {
      this.props.history.push('/escuelas');
    }
  };

  onGuardarHandler = () => {
    this.props.history.push('/escuelas');
  }

  onCancelarHandler = () => {
    this.props.history.push('/escuelas');
  }

  render() {
    let infoAlumno = this.props.location.alumno;
    let infoGrado = this.props.location.grado;

    return (
      <Container style={{ padding: '0px' }}>
        <Alumno datos={infoAlumno} grado={infoGrado} />
        <GuardarCancelar
          guardar={this.onGuardarHandler}
          cancelar={this.onCancelarHandler} />
      </Container>
    )
  }
}

export default DetalleAlumno;
