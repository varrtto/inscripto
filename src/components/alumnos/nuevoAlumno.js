import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Alumno from './alumno/alumno';
import GuardarCancelar from '../UI/Botoneras/guardarCancelar';

class NuevoAlumno extends Component {
  componentWillMount() {
    if (!this.props.location.grado) {
      this.props.history.push('/escuelas');
    }
  };

  onGuardarHandler = () => {
    this.props.history.push('/escuelas');
  }

  onCancelarHandler = () => {
    this.props.history.push('/escuelas');
  }

  render() {
    let infoGrado = this.props.location.grado;
    return (
      <Container style={{ padding: '0px' }}>
        <Alumno grado={infoGrado} />
        <GuardarCancelar
          guardar={this.onGuardarHandler}
          cancelar={this.onCancelarHandler} />
      </Container>
    )
  }
}

export default NuevoAlumno;
