import React, { Component } from 'react';
import { Form, Container } from 'react-bootstrap';
import Placeholder from '../../UI/placeholder/placeholder';
import GuardarCancelar from '../../UI/Botoneras/guardarCancelar';

class Curso extends Component {
  onGuardarHandler = () => {
    this.props.history.push('/escuelas');
  }

  onCancelarHandler = () => {
    this.props.history.push('/escuelas');
  }

  render() {
    let datosCurso = {
    }
    if (this.props.datos) {
      datosCurso = this.props.datos;
    }
    return (
      <Form>
        <Container style={{ borderLeft: '6px solid pink' }}>
          <br />
          <p style={{ textAlign: 'left' }}>Datos Curso</p>
          <Form.Row>
            <Form.Group className='col-md' controlId="formGridTurno">
              <Placeholder>Turno</Placeholder>
              <Form.Control type="number" defaultValue={datosCurso.turno} />
            </Form.Group>
            <Form.Group className='col-md' controlId="formGridAnio">
              <Placeholder>Año</Placeholder>
              <Form.Control type="number" defaultValue={datosCurso.anio} />
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group className='col-md' controlId="formGridDivision">
              <Placeholder>Division</Placeholder>
              <Form.Control type="number" defaultValue={datosCurso.division} />
            </Form.Group>
            <Form.Group className='col-md' controlId="formGridSalon">
              <Placeholder>Salon</Placeholder>
              <Form.Control type="number" defaultValue={datosCurso.salon} />
            </Form.Group>
          </Form.Row>
          <GuardarCancelar
            guardar={this.onGuardarHandler}
            cancelar={this.onCancelarHandler} />
        </Container>
      </Form>
    )
  }
}

export default Curso;
