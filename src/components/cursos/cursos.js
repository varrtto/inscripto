import React, { Component } from 'react';
import { Table, Container } from 'react-bootstrap';
import { withRouter, Link } from 'react-router-dom'

const cursos = [
  {
    id: 'a',
    anio: 1,
    division: 1,
    turno: 'mañana',
    salon: 1,
  },
  {
    id: 'b',
    anio: 2,
    division: 1,
    turno: 'mañana',
    salon: 2,
  },
  {
    id: 'c',
    anio: 3,
    division: 1,
    turno: 'mañana',
    salon: 3,
  }
];

class Cursos extends Component {

  componentWillMount() {
    if (!this.props.numero && !this.props.location.numero) {
      this.props.history.push('/escuelas');
    }
  };

  render() {
    let toCurso = {};
    let elEstilo = {
      all: 'unset',
      display: 'block'
    }

    let listaCursos = Object.values(cursos).map(curso => {
      toCurso = {
        pathname: this.props.match.url.concat('/curso/' + curso.anio + '/' + curso.division + '/' + curso.turno),
        anio: curso.anio,
        division: curso.division,
        turno: curso.turno,
        escuela: this.props.location.numero
      }
      return (
        <tr
          style={{ cursor: 'pointer' }}
          key={curso.anio + curso.division + curso.turno}>
          <td><Link to={toCurso} style={elEstilo}>{curso.turno}</Link></td>
          <td><Link to={toCurso} style={elEstilo}>{curso.anio}</Link></td>
          <td><Link to={toCurso} style={elEstilo}>{curso.division}</Link></td>
          <td><Link to={toCurso} style={elEstilo}>{curso.salon}</Link></td>
        </tr>
      )
    });

    return (
      <Container style={{ textAlign: 'center', padding: '0px' }}>
        <h3>Cursos escuela N. {this.props.location.numero}</h3>
        <Table striped hover responsive size='sm'>
          <thead>
            <tr style={{ backgroundColor: 'lightblue' }}>
              <th>Turno</th>
              <th>Año</th>
              <th>Division</th>
              <th>Salon</th>
            </tr>
          </thead>
          <tbody>
            {listaCursos}
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default withRouter(Cursos);
