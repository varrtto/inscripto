import React from 'react';
import {Container} from 'react-bootstrap';

const documento = (props) => {
    if (!props.location.titulo) {
        props.history.push('/documentos')
    }
    return (
        <Container>
            <h1>
                {props.location.titulo}
            </h1>
            <h2>
                {props.location.descripcion}
            </h2>
            <p>
                {props.location.texto}
            </p>
        </Container>
    )
}

export default documento;