import React from 'react';
import { Card, Nav, Container, CardColumns } from 'react-bootstrap';
import { withRouter, Link } from 'react-router-dom';

const listaDocumentos = [{
  numero: 1,
  titulo: 'untitulo',
  descripcion: 'El juez Maximiliano Camarda rechazó el pedido de la querella de apartar al magistrado para tratar el pedido de la defensa.',
  texto: ''
}, {
  numero: 2,
  titulo: 'otroTitulo',
  descripcion: 'Juntos Somos Río Negro presentó anoche su lista para las elecciones provinciales del 7 de abril. Diez legisladores van por la reelección en sus bancas.',
  texto: ''
}, {
  numero: 3,
  titulo: 'masTitulos',
  descripcion: 'Las tres primeras fuerzas lanzaron su primer despliegue de folletería e instalaron puestos de información. Otros partidos vienen más rezagados.',
  texto: ''
}]

const documentos = (props) => {

  let toDocumento = {}

  let listdocumentos = Object.values(listaDocumentos).map(documento => {
    toDocumento = {
      pathname: "/documentos/".concat(documento.numero),
      numero: documento.numero,
      titulo: documento.titulo,
      descripcion: documento.descripcion,
      texto: documento.texto
    }
    return (
      <Nav.Link
        key={documento.numero}
        as={Link}
        to={toDocumento}
        style={{ all: 'unset' }}>
        <Card
          style={{ width: '100%', margin: '3px 3px', cursor: 'pointer' }}>
          <Card.Header style={{ backgroundColor: 'lightblue' }}> {documento.titulo} </Card.Header>
          <Card.Body>
            <Card.Text>
              <br />
              {documento.descripcion}
            </Card.Text>
          </Card.Body>
        </Card>
      </Nav.Link>
    )
  })

  return (
    <Container>
      <h1>Lista de documentos</h1>
      <CardColumns style={{ width: '100%', 'justifyContent': 'center', display: 'inline-block' }}>
        {listdocumentos}
      </CardColumns>
    </Container>
  )

};

export default withRouter(documentos);
