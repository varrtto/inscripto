import React from 'react';
import { Form, Container } from 'react-bootstrap';
import Placeholder from '../../UI/placeholder/placeholder';

const escuela = (props) => {
  let datosEscuela = {
    direccion: {}
  }
  if (props.datos && props.datos.numero) {
    datosEscuela = props.datos;
  }

  return (
    <Form>
      <Container style={{ borderLeft: '6px solid lightblue' }}>
        <br />
        <p style={{ textAlign: 'left' }}>Datos Escuela</p>
        <Form.Row>
          <Form.Group className='col-md' controlId="formGridNumero">
            <Placeholder>Numero</Placeholder>
            <Form.Control type="number" defaultValue={datosEscuela.numero} />
          </Form.Group>
          <Form.Group className='col-md' controlId="formGridNombre">
            <Placeholder>Nombre</Placeholder>
            <Form.Control type="text" defaultValue={datosEscuela.nombre} />
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridLocalidad">
            <Placeholder>Localidad</Placeholder>
            <Form.Control type="text" defaultValue={datosEscuela.localidad} />
          </Form.Group>
          <Form.Group className='col-md' controlId="formGridCalle">
            <Placeholder>Calle</Placeholder>
            <Form.Control type="text" defaultValue={datosEscuela.direccion.calle} />
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridNumeroDir">
            <Placeholder>NumeroDir</Placeholder>
            <Form.Control type="number" defaultValue={datosEscuela.direccion.numero} />
          </Form.Group>
          <Form.Group className='col-md' controlId="formGridTelefono">
            <Placeholder>Telefono</Placeholder>
            <Form.Control type="number" defaultValue={datosEscuela.telefono} />
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group className='col-md' controlId="formGridDescripcion">
            <Placeholder>Descripcion</Placeholder>
            <Form.Control as="textarea" type="text" defaultValue={datosEscuela.descripcion} />
          </Form.Group>
        </Form.Row>
        <br />
      </Container>
    </Form>
  )
}


export default escuela;
