import React from 'react';
import { Container } from 'react-bootstrap';
import ListaEscuelas from './listaEscuelas';

const listaEscuelas = [{
  numero: 274,
  nombre: 'unNombre',
  localidad: 'San Martin de los Andes',
  descripcion: 'Escuela 274 de San Martin de los Andes',
  direccion: {
    calle: 'Villegas',
    numero: 434,
    codigoPostal: 8500
  }
}, {
  numero: 5,
  nombre: 'otroNombre',
  localidad: 'San Martin de los Andes',
  descripcion: 'Escuela 5 de San Martin de los Andes',
  direccion: {
    calle: 'Av. San Martin',
    numero: 500,
    codigoPostal: 8500
  }
}, {
  numero: 13,
  nombre: 'masNombres',
  localidad: 'San Martin de los Andes',
  descripcion: 'Escuela 13 de San Martin de los Andes',
  direccion: {
    calle: 'Cnel. Diaz',
    numero: 733,
    codigoPostal: 8500
  }
}]

const escuelas = (props) => {
  let modificar = "false"
  if (props.modificar === "true") {
    modificar = props.modificar
  }

  return (
    <Container>
      <h1>Lista de Escuelas</h1>
      <ListaEscuelas lista={listaEscuelas} modificar={modificar} />
    </Container>
  )

};

export default escuelas;
