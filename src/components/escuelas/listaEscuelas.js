import React from 'react';
import { Card, Nav, CardColumns } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const listaEscuelas = (props) => {
    let toEscuela = {};
    let ruta = "/escuelas/";
    if (props.modificar === "true") {
        ruta = "/administracion/"
    }

    let listescuelas = Object.values(props.lista).map(escuela => {
        toEscuela = {
            pathname: ruta.concat(escuela.numero),
            numero: escuela.numero,
            nombre: escuela.nombre,
            localidad: escuela.localidad,
            descripcion: escuela.descripcion,
            direccion: {
              calle: escuela.direccion.calle,
              numero: escuela.direccion.numero,
              codigoPostal: escuela.direccion.codigoPostal
            }
        }

        return (
            <Nav.Link
                key={escuela.numero}
                as={Link}
                to={toEscuela}
                style={{ all: 'unset' }}>
                <Card
                    style={{ width: '100%', margin: '3px 3px', cursor: 'pointer' }}>
                    <Card.Header style={{ backgroundColor: 'lightblue' }}> {escuela.numero} </Card.Header>
                    <Card.Body>
                        <Card.Title> {escuela.nombre} </Card.Title>
                        <Card.Text style={{ textAlign: 'left' }}>
                            <br />
                            Calle: {escuela.direccion.calle}
                            <br />
                            Numero: {escuela.direccion.numero}
                            <br />
                            C.P.: {escuela.direccion.codigoPostal}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Nav.Link>
        )
    })
    return (
        <CardColumns style={{ width: '100%', 'justifyContent': 'center', display: 'inline-block' }}>
            {listescuelas}
        </CardColumns>
    )
}

export default listaEscuelas;