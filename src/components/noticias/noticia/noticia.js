import React from 'react';
import {Container} from 'react-bootstrap';

const noticia = (props) => {
    if (!props.location.titulo) {
        props.history.push('/noticias')
    }
    return (
        <Container>
            <h1>
                {props.location.titulo}
            </h1>
            <h2>
                {props.location.copete}
            </h2>
            <p>
                {props.location.noticia}
            </p>
        </Container>
    )
}

export default noticia;