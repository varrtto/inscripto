import React from 'react';
import { Card, Nav, Container, CardColumns} from 'react-bootstrap';
import { withRouter, Link } from 'react-router-dom';

const listaNoticias = [{
  numero: 1,
  localidad: 'Neuquen',
  titulo: 'untitulo',
  copete: 'El juez Maximiliano Camarda rechazó el pedido de la querella de apartar al magistrado para tratar el pedido de la defensa.',
  nota: ''
},{
  numero: 2,
  localidad: 'San Martin de los Andes',
  titulo: 'otroTitulo',
  copete: 'Juntos Somos Río Negro presentó anoche su lista para las elecciones provinciales del 7 de abril. Diez legisladores van por la reelección en sus bancas.',
  nota: ''
},{
  numero: 3,
  localidad: 'Cutral Co',
  titulo: 'masTitulos',
  copete: 'Las tres primeras fuerzas lanzaron su primer despliegue de folletería e instalaron puestos de información. Otros partidos vienen más rezagados.',
  nota: ''
}]

  const noticias = (props) => {

  let toNoticia = {}

  let listnoticias = Object.values(listaNoticias).map(noticia => {
    toNoticia = {
      pathname: "/noticias/".concat(noticia.numero),
      numero: noticia.numero,
      localidad: noticia.localidad,
      titulo: noticia.titulo,
      copete: noticia.copete,
      nota: noticia.nota
    }
    return (
        <Nav.Link
          key={noticia.numero}
          as={Link}
          to={toNoticia}
          style={{all: 'unset'}}>
          <Card
            style={{ width: '100%', margin: '3px 3px', cursor: 'pointer'}}>
            <Card.Header style={{backgroundColor: 'lightblue'}}> {noticia.localidad} </Card.Header>
            <Card.Body>
              <Card.Title> {noticia.titulo} </Card.Title>
              <Card.Text>
                <br />
                {noticia.copete}
              </Card.Text>
            </Card.Body>
          </Card>
        </Nav.Link>
    )
  })

  return (
    <Container>
      <h1>Lista de noticias</h1>
      <CardColumns style={{ width: '100%', 'justifyContent': 'center', display: 'inline-block'}}>
        {listnoticias}
      </CardColumns>
    </Container>
  )

};

export default withRouter(noticias);
